export const navOptions = [
  { text: "Acerca de", hash: "#about" },
  { text: "Estudios y certificados", hash: "#certificates" },
  { text: "Habilidades", hash: "#skills" },
  { text: "Proyectos", hash: "#projects" },
];
export const contents = {
  sections: [
    {
      id: "profile",
      title: "Full-Stack Developer & Freelancer",
      background: false,
      fullname: "Leonardo Campo",
      photoUrl: "/img/photo_mQy8C7Qocg.jpg",
      social: [
        { icon: "mdi-linkedin", url: "https://www.linkedin.com/in/leocmpro" },
        { icon: "mdi-gitlab", url: "https://www.gitlab.com/leocmpro" },
      ],
    },
    {
      id: "about",
      title: "Acerca de mí",
      background: true,
      content: [
        `¡Hola! Soy Leonardo Campo un apasionado desarrollador Full-Stack con 
        más 12 años de experiencia en el desarrollo de proyectos de software
        modernos y escalables. He liderado equipos y contribuído al éxito de
        diversos proyectos.`,
        `Estoy comprometido con la excelencia y siempre estoy dispuesto a
        enfrentar nuevos desafíos que me permitan seguir aprendiendo y creciendo
        como profesional y como persona.`,
      ],
    },
    {
      id: "certificates",
      title: "Estudios y certificados",
      background: false,
      education: [
        {
          date: "Abr 2021",
          company: "Uniremington",
          title: "Especialización en Seguridad de la Información",
          country: "Colombia",
        },
        {
          date: "May 2019",
          company: "Uniremington",
          title: "Ingeniería de Sistemas",
          country: "Colombia",
        },
        {
          date: "Abr 2011",
          company: "Universidad del Valle",
          title: "Tecnología en Sistemas de Información",
          country: "Colombia",
        },
      ],
      certificates: [
        {
          img: "/img/scrum-master-professional-certificate-smpc.1.png",
          url: "https://www.credly.com/badges/c8b873bd-0bca-4db5-93d5-18638bda96a1/public_url",
        },
        {
          img: "/img/scrum-developer-professional-certification-sdpc.png",
          url: "https://www.credly.com/badges/51865e03-283e-4b5f-b162-3826667fd449/public_url",
        },
        {
          img: "/img/scrum-product-owner-professional-certificate-spopc.1.png",
          url: "https://www.credly.com/badges/4beae03f-640c-407a-9bd0-b1f5ed103f26/public_url",
        },
        {
          img: "/img/scrum-foundation-professional-certification-sfpc.png",
          url: "https://www.credly.com/badges/2c051f52-5412-47d6-8bae-56cab7970341/public_url",
        },
        {
          img: "/img/lifelong-learning.png",
          url: "https://www.credly.com/badges/6c9df695-fafa-4256-8ee1-01878253b2c7/public_url",
        },
      ],
    },
    {
      id: "skills",
      title: "Habilidades",
      background: true,
      skills: [
        "Java",
        "Python",
        "Vue.js/Nuxt.js",
        "PostgreSQL",
        "Scrum",
        "UI/UX",
        "Flask",
        "Vuetify",
        "Socket.IO",
        "HTML",
        "CSS",
        "Javascript",
        "jQuery",
        "MariaDB",
        "MS SQL Server",
        "MongoDB",
        "RedisDB",
        "TimescaleDB",
        "GNU/Linux",
        "Docker/Docker Compose",
        "Hardening y cybersecurity",
        "OWASP",
        "ethical hacking",
        "SeleniumBase",
        "Playwright",
        "capacitor.js",
        "Pandas",
        "Open API Specification",
        "API Swagger",
        "API REST",
        "Nginx",
        "Raspberry Pi",
        "Arduino",
        "Redes de datos",
        "pfSense",
      ],
    },
    {
      id: "projects",
      title: "Proyectos",
      background: false,
      description: `Estos son aglunos de los proyectos más representativos en 
      los que he trabajado, algunos son de gran complejidad y otros 
      corresponden automatizaciones o módulos`,
      projects: [
        {
          name: "SGP",
          company: "Plasti Films Internacional S.A.",
          description: `Software hecho a la medida para realizar la gestión de 
          los procesos de la compañía.`,
          tecnologies: [
            "Java",
            "JSP",
            "Struts",
            "HTML",
            "Javascript",
            "jQuery",
            "jQueryUI",
            "PostgreSQL",
            "GNU/Linux",
          ],
        },
        {
          name: "Control de piso - SGP",
          company: "Plasti Films Internacional S.A.",
          description: `Proyecto de automatización industrial (IIoT) y módulos 
          para monitoreo y control en tiempo real del máquinas insdustriales 
          involucradas en el proceso de producción.`,
          tecnologies: [
            "Java",
            "Python",
            "Nuxt.js/Vue.js",
            "Flask",
            "Raspberry Pi",
            "JSP",
            "Struts",
            "PostgreSQL",
            "RedisDB",
            "GNU/Linux",
          ],
        },
        {
          name: "Passive DNS Platform (Freelance)",
          company: "Edgewatch - Occentus Network",
          description: `Plataforma con API REST documentada que ofrece el 
          servicio de Passive DNS. **Proyecto en curso.`,
          tecnologies: [
            "Python",
            "API REST",
            "PostgreSQL",
            "RedisDB",
            "MongoDB",
            "GNU/Linux",
          ],
        },
      ],
    },
  ],
};
