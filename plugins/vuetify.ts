// import this after install `@mdi/font` package
import "@mdi/font/css/materialdesignicons.css";

import "@/assets/main.scss";
import { createVuetify } from "vuetify";

export default defineNuxtPlugin((nuxtApp) => {
  const vuetify = createVuetify({
    ssr: true,
    defaults: {
      VBtn: {
        rounded: "lg",
      },
      VListItem: {
        rounded: "lg",
      },
    },
    theme: {
      defaultTheme: "dark",
      themes: {
        dark: {
          dark: true,
          colors: {
            background: "#3D3B40",
            surface: "#3D3B40",
            primary: "#525CEB",
            background2: "#BFCFE7",
          },
        },
        light: {
          dark: false,
          colors: {
            background: "#BFCFE7",
            surface: "#BFCFE7",
            primary: "#525CEB",
          },
        },
      },
    },
  });
  nuxtApp.vueApp.use(vuetify);
});
