import vuetify, { transformAssetUrls } from "vite-plugin-vuetify";

const { CI_PAGES_URL } = process.env;
const baseURL = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname;

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },

  ssr: true,

  app: {
    buildAssetsDir: "/_assets/",
    baseURL,
  },

  webpack: {
    extractCSS: {
      ignoreOrder: true,
    },
  },

  router: {
    options: {
      scrollBehaviorType: "smooth",
    },
  },

  dir: {
    public: "static",
  },

  // vuetify
  build: {
    transpile: ["vuetify"],
  },
  modules: [
    "@nuxt/eslint",
    (_options, nuxt) => {
      nuxt.hooks.hook("vite:extendConfig", (config) => {
        // @ts-expect-error: typescript syntax error fix
        config.plugins.push(vuetify({ autoImport: true }));
      });
    },
  ],
  vite: {
    vue: {
      template: {
        transformAssetUrls,
      },
    },
  },

  // https://nitro.unjs.io/config#output
  nitro: {
    output: {
      publicDir: 'public'
    }
  }
});
